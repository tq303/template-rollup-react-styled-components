import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import replace from 'rollup-plugin-replace';
import resolve from 'rollup-plugin-node-resolve';
import serve from 'rollup-plugin-serve';
import livereload from 'rollup-plugin-livereload';

const isWatching = process.env.ROLLUP_WATCH === 'true';

export default {
  input: 'src/index.jsx',
  output: {
    file: 'public/main.js',
    format: 'iife',
  },
  watch: {
    include: 'src/**',
  },
  plugins: [
    babel({
      presets: [
        '@babel/preset-env',
        '@babel/preset-react',
      ],
      exclude: 'node_modules/**',
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
    }),
    resolve({
      browser: true,
      dedupe: [
        'react',
        'react-dom',
        'styled-components',
      ],
    }),
    commonjs({
      include: 'node_modules/**',
      namedExports: {
        'node_modules/react/index.js': [
          'cloneElement',
          'createContext',
          'Component',
          'createElement'
        ],
        'node_modules/react-dom/index.js': ['render', 'hydrate'],
        'node_modules/react-is/index.js': [
          'isElement',
          'isValidElementType',
          'ForwardRef'
        ]
      },
    }),
    isWatching && serve({
      open: true,
      contentBase: 'public',
      port: 8080,
      historyApiFallback: true,
    }),
    isWatching && livereload('public'),
  ],
};