import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';

const HelloWorld = styled.div`
  background: red;
  color: white;
  height: 200px;
  width: 200px;
`;

const rootElement = document.createElement('div');
rootElement.setAttribute('id', 'root');

window.addEventListener('load', () => {
  ReactDOM.render(
    <HelloWorld />,
    document.body.appendChild(rootElement),
  );
});